import React, { useState } from 'react';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import axios from 'axios';

function Form() {
    // Initial state
    const [formData, setFormData] = useState([{ id: 1, lastName: '', firstName: '', age: '' }]);
    const [nextId, setNextId] = useState(2);

    const addRow = () => {
        const newRow = { id: nextId, lastName: '', firstName: '', age: '' };
        setNextId(nextId + 1);
        setFormData([...formData, newRow]);
    };

    const handleChange = (id, field, value) => {
        const updatedFormData = formData.map((row) =>
            row.id === id ? { ...row, [field]: value } : row
        );
        setFormData(updatedFormData);
    };

    const handleSubmit = async (e) => {
        e.preventDefault();

        try {
            const response = await axios.post('/send-data', { rows: formData }, {
                headers: {
                    'Content-Type': 'application/json',
                },
            });

            console.log('Response:', response); // Debugging: Log the response to inspect its structure

            if (response && response.status === 200) {
                console.log('Data successfully sent to the server');
                // Optionally, reset the form or perform other actions
            } else {
                console.error('Failed to send data to the server');
            }
        } catch (error) {
            console.error('Error sending data:', error);
        }
    };

    return (
        <div>
            <Typography variant="h5">Form with Rows</Typography>
            <form onSubmit={handleSubmit}>
                <Grid container spacing={2}>
                    {formData.map((row) => (
                        <Grid container item spacing={2} key={row.id}>
                            <Grid item xs={2}>
                                <TextField
                                    fullWidth
                                    label="Last Name"
                                    value={row.lastName}
                                    onChange={(e) => handleChange(row.id, 'lastName', e.target.value)}
                                />
                            </Grid>
                            <Grid item xs={2}>
                                <TextField
                                    fullWidth
                                    label="First Name"
                                    value={row.firstName}
                                    onChange={(e) => handleChange(row.id, 'firstName', e.target.value)}
                                />
                            </Grid>
                            <Grid item xs={2}>
                                <TextField
                                    fullWidth
                                    label="Age"
                                    value={row.age}
                                    onChange={(e) => handleChange(row.id, 'age', e.target.value)}
                                />
                            </Grid>
                        </Grid>
                    ))}
                </Grid>
                <Button variant="contained" onClick={addRow}>
                    Add Row
                </Button>
                <Button type="submit" variant="contained" color="primary">
                    Submit
                </Button>
            </form>
        </div>
    );
}

export default Form;